const menu = document.querySelector('.navTrigger')
const mobileNav = document.querySelector('.mobile-nav')
const subMenuLink = document.querySelector('.has-submenu');
const subMenu = document.querySelector('.submenu')
const mobileSubMenuLink = document.querySelector('.mobile-have-submenu')
const mobileNavActive = document.querySelector('.mobile-nav active')
const logo = document.querySelector('.logo-block')
const subMenuArrow = document.querySelector('.mobile-have-submenu .sub-submenu-arrow')


menu.addEventListener('click', ()=>{
  if (mobileNav.style.display === "none") {
    mobileNav.style.display = "block";
    setTimeout(()=>{
      mobileNav.classList.toggle('active')
    },150)
  } else {
    mobileNav.classList.toggle('active')
    setTimeout(()=>{
      mobileNav.style.display = "none";
    },500)
  }
  menu.classList.toggle('active')
  logo.classList.toggle('hidden')
  if(menu.classList.contains('active')){
    document.querySelector('.show').style.display = 'none'
  }else{
    document.querySelector('.show').style.display = 'block'
  }
})

mobileSubMenuLink.addEventListener('click', function(){
  mobileSubMenuLink.classList.toggle('active')
  subMenuArrow.classList.toggle('active')
  if(this.classList.contains('active')){
    mobileNav.style.height='91vh';
  }else{
    mobileNav.style.height="100vh"
  }
})

$('.search-header').click(function(e){
  e.preventDefault()
  $('.search-popup').toggleClass('active')
  $('.form-wrapper').toggleClass('active')
  $('.search-input').focus()
  if($('.search-popup').hasClass('active')){
    $('body').css({
      overflow: 'hidden',
    })
  }
})

$('.search-popup').click(function(e){
  e.preventDefault()
  
  if (e.target.classList.contains('close-search') || e.target.classList.contains('search-popup')){
    $('.search-popup').toggleClass('active')
    $('.form-wrapper').toggleClass('active')
    $('.search-form')[0].reset();
    $('.shadow-form')[0].reset()
    $('body').css({
      overflow: 'unset',
    })
  }  
})

$( ".search-input" )
  .keyup(function() {
    $('.form-text').val($(this).val())
  })

  $('.law-expend').click(function(e){
    e.preventDefault()
    $(this).toggleClass('active')
    $('.law-expended-text').toggleClass('active')
  })